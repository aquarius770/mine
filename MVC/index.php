<?php

require_once('controller.php');
require_once('model.php');


$op = $_POST['op'];

$user_controller = new UserController();

if (isset($op)) {
    $username = $_POST['user'];
    $password = $_POST['pass'];

    if($user_controller->login ($username, $password)){

        // echo "i'm in index";
        header("Location:main.php");
    } else {
        header("Location:login.php?err=1");
    }
}

if (isset($_GET['op'])) {
    $user_controller->logout();
    header("Location:login.php");
}


?>